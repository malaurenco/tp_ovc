using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sliderMouseSensi : MonoBehaviour
{
    public GameObject _eye;
    // Start is called before the first frame update
    void Start()
    {
        float val = PlayerPrefs.GetFloat("mouseSens", 7.0f);
        GetComponent<Slider>().value = val;
        _eye.GetComponent<EyeMovement>().setSensi(GetComponent<Slider>().value);
    }

    // Update is called once per frame
    void Update()
    {
        _eye.GetComponent<EyeMovement>().setSensi(GetComponent<Slider>().value);
        PlayerPrefs.SetFloat("mouseSens", GetComponent<Slider>().value);
        
    }


}
