using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class MenuCreditSelector : MonoBehaviour
{
    public TMP_Text _wallText;

    private bool _isCredit;
    private Vector3 _pos;
    private string _text;
    // Start is called before the first frame update
    void Start()
    {
        int max = PlayerPrefs.GetInt("maxScore", 0);
        _wallText.text = "The high score is :\n " + max;
        _isCredit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(_isCredit)
        {
            _wallText.gameObject.transform.position = _wallText.gameObject.transform.position + Vector3.up * 1.0f*Time.deltaTime;
            if(_wallText.gameObject.transform.position.y > _pos.y + 10)
            {
                _isCredit = false;
                _wallText.gameObject.transform.position = _pos;
                _wallText.text = _text;
                _wallText.color = Random.ColorHSV();

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        if (!_isCredit)
        {
            _pos = _wallText.gameObject.transform.position;
            _text = _wallText.text;
            _wallText.text = "Thanks for playing\nUndercooked\n\nCreated by:\nAll�gre Mat�o\nLauren�ot Mathieu\n\nwith help from:\nRoyer Jules\nMesnard Emmanuel\n\nUsing the following\nAssets from the\nUnity Asset Store:\n\nFast Food Restaurant Kit\nby Brick Project Studio\n\nQuick Outline\nby Chris Nolet\n\nAutomatic Revolving Door\nby DevDen\n\n\nTHANKS";
            _wallText.gameObject.transform.position = _wallText.gameObject.transform.position + Vector3.up * -10.0f;
            _wallText.color = Random.ColorHSV();
            _isCredit = true;
        }
        else
        {
            _isCredit = false;
            _wallText.gameObject.transform.position = _pos;
            _wallText.text = _text;
            _wallText.color = Random.ColorHSV();
        }
    }
}
