using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class BurgerGenAndVerif : MonoBehaviour
{

    enum Ingredient
    {
        PainHaut,
        PainBas,
        Steak,
        Salade,
        Tomates,
        Fromage
    }

    public GameObject[] _prefabsIngredients;
    public GameObject _particule;
    public Transform _baseBurger;
    public GameObject _textScore;
    private Ingredient[] _burger;
    private int _burgerSize;
    private float _getTime;

    // Start is called before the first frame update
    void Start()
    {
        _particule.GetComponent<ParticleSystem>().Stop();
        GenerateBurger();
        InstanciateBurger();
    }

    // Update is called once per frame
    void Update()
    {
        _getTime += Time.deltaTime;
    }

    void GenerateBurger()
    {
        _burgerSize = Random.Range(4, 9);
        _burger = new Ingredient[_burgerSize];
        _burger[0] = Ingredient.PainBas;
        for(int i = 1; i < _burgerSize - 1; ++i)
        {
            do
            {
                _burger[i] = (Ingredient)Random.Range(1, System.Enum.GetValues(typeof(Ingredient)).Length);
            } while (_burger[i] == _burger[i - 1]);
        }
        _burger[_burgerSize - 1] = Ingredient.PainHaut;
        _getTime = 1;
    }

    void InstanciateBurger()
    {
        Transform previousIngredient = _baseBurger;
        foreach(Ingredient i in _burger)
        {
            previousIngredient = Instantiate(_prefabsIngredients[(int)i], previousIngredient).transform;
            previousIngredient.Rotate(0, Random.Range(0.0f,360.0f), 0);
            previousIngredient = previousIngredient.GetChild(0);
        }
    }

    public void VerifyBurger(GameObject plateau)
    {
        GameObject nextPose = plateau.GetComponent<Transform>().Find("NextPose").gameObject;
        GameObject ingredient;
        var burgerDansPlateau = new List<Ingredient>();

        while(nextPose.transform.childCount > 0)
        {
            ingredient = nextPose.transform.GetChild(0).gameObject;
            burgerDansPlateau.Add((Ingredient)ingredient.GetComponent<IngredientId>().GetId());
            nextPose = ingredient.transform.GetChild(0).gameObject;
        }

        if (_burger.SequenceEqual(burgerDansPlateau))
        {
            if(_getTime < 100)
            {
                _textScore.GetComponent<LevelTimerAndScore>().addScore((int)(_burgerSize * (100.0f/_getTime)));
                Invoke(nameof(ColorChange), 3);
            }
            else
            {
                _textScore.GetComponent<LevelTimerAndScore>().addScore(_burgerSize);
                Invoke(nameof(ColorChange), 2);
            }
            _particule.GetComponent<ParticleSystem>().Play();
            _textScore.GetComponent<TMP_Text>().color = Color.green;
            Invoke(nameof(closeParticules), 1);
        }
        else
        {
            _textScore.GetComponent<TMP_Text>().color = Color.red;
            Invoke(nameof(ColorChange), 3);
        }
    }

    public void RegenerateBurger()
    {
        Destroy(_baseBurger.transform.GetChild(0).gameObject);
        GenerateBurger();
        InstanciateBurger();
    }

    private void ColorChange()
    {
        _textScore.GetComponent<TMP_Text>().color = Color.white;
    }

    private void closeParticules()
    {
        _particule.GetComponent<ParticleSystem>().Stop();
    }
}
