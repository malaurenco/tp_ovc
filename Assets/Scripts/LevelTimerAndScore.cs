using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTimerAndScore : MonoBehaviour
{
    public int _levelTime = 300;
    public int _score;
    public float _time;
    // Start is called before the first frame update
    void Start()
    {
        _score = 0;
        _time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        if(_time >= _levelTime)
        {
            int max = PlayerPrefs.GetInt("maxScore", 0);
            if(max < _score) {
                PlayerPrefs.SetInt("maxScore", _score);
            }
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
            /*Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;*/
        }
        GetComponent<TMP_Text>().text = "Time " + (int)(_levelTime - _time) + "\nScore : " + _score;
    }

    public void addScore(int score)
    {
        _score += score;
    }
}
